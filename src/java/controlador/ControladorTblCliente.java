package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entidades.Catalogo;
import entidades.TblCliente;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

@WebServlet(name = "ControladorTblCliente", urlPatterns = {"/ControladorTblCliente"})

public class ControladorTblCliente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void imprimir(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            TblCliente tblCliente = new TblCliente();
            Iterable<TblCliente> lista;
            lista = tblCliente.Listar();

            out.println("<table class='table table-hover table-inverse'>");

            out.println("<tr>"
                    + "<td> ID  </td>"
                    + "<td> nombre </td>"
                    + "<td> apellido</td>"
                    + "<td> clave</td>"
                    + "<td> estado</td>"
                    + "</tr>");
            for (TblCliente contenido : lista) {
                out.println("<tr>"
                        + "<td>" + contenido.getPkIdCliente() + "</td>"
                        + "<td>" + contenido.getNombreCliente() + "</td>"
                        + "<td>" + contenido.getApellidoCliente() + "</td>"
                        + "<td>" + contenido.getClaveCliente() + "</td>"
                        + "<td>" + contenido.getCatalogo().getEstado() + "</td>"
                        + "</tr>");
                //       System.out.println(contenido);
            }
            out.print("</table>");
        } catch (IOException ex) {
            Logger.getLogger(ControladorTblCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String operacion = request.getParameter("operacion");

        switch (operacion) {
            case "insertar":
                insertar(request, response);
                break;
            default:
                break;
        }
        imprimir(request, response);
    }

    public void insertar(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("CONTROLADOR CLIENTE INSERTAR");

        TblCliente tblCliente = new TblCliente();
        tblCliente.setPkIdCliente(String.valueOf(tblCliente.maximoId()));
        // tblCliente.setPkIdCliente(request.getParameter("id"));
        tblCliente.setApellidoCliente(request.getParameter("apellido"));
        tblCliente.setClaveCliente(request.getParameter("clave"));
        tblCliente.setNombreCliente(request.getParameter("nombre"));
        Catalogo catalogo = new Catalogo();
        System.out.println(request.getParameter("estado") + " estado");
        tblCliente.setCatalogo(catalogo.buscar((request.getParameter("estado"))));
        tblCliente.insertar(tblCliente);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
