package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entidades.Catalogo;
import entidades.TblCliente;
import entidades.TblCombo;
import entidades.TblDetalle;
import entidades.TblFormaPago;
import entidades.TblProducto;
import entidades.TblVendedor;
import entidades.TblVenta;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

@WebServlet(name = "ControladorTblCliente", urlPatterns = {"/ControladorTblCliente"})

public class ControladorTblVenta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {

    }

    protected void imprimir(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            TblCliente tblCliente = new TblCliente();
            Iterable<TblCliente> lista;
            lista = tblCliente.Listar();

            out.println("<table class='table table-hover table-inverse'>");

            out.println("<tr>"
                    + "<td> ID  </td>"
                    + "<td> nombre </td>"
                    + "<td> apellido</td>"
                    + "<td> clave</td>"
                    + "</tr>");
            for (TblCliente contenido : lista) {
                out.println("<tr>"
                        + "<td>" + contenido.getPkIdCliente() + "</td>"
                        + "<td>" + contenido.getNombreCliente() + "</td>"
                        + "<td>" + contenido.getApellidoCliente() + "</td>"
                        + "<td>" + contenido.getClaveCliente() + "</td>"
                        + "</tr>");
                //       System.out.println(contenido);
            }
            out.print("</table>");
        } catch (IOException ex) {
            Logger.getLogger(ControladorTblVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String operacion = request.getParameter("operacion");

        switch (operacion) {
            case "insertar":
                insertar(request, response);
                break;
            default:
                break;
        }
        imprimir(request, response);
    }

    public void insertar(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("CONTROLADOR CLIENTE INSERTAR");
        /*      1, //      int idCatalogo,
                3, //      idCliente
                1, //      idCombo
                222,//     idproducto
                7, //     cantidadDetalle
                1, //      tipoDescuentoDetalle
                1, //      idFormaPago
                30 //     tblVendedor
                ); // fechaVenta
        */

        int idCatalogo = Integer.parseInt(request.getParameter("idCatalogo"));
        int idCliente = Integer.parseInt(request.getParameter("idCliente"));
        int idCombo = Integer.parseInt(request.getParameter("idCombo"));
        int idproducto = Integer.parseInt(request.getParameter("idproducto"));
        int cantidadDetalle = Integer.parseInt(request.getParameter("cantidadDetalle"));
        int tipoDescuentoDetalle = Integer.parseInt(request.getParameter("tipoDescuentoDetalle"));
        int idFormaPago = Integer.parseInt(request.getParameter("idFormaPago"));
        int idVendedor = Integer.parseInt(request.getParameter("tblVendedor"));

        Date myDate = new Date();
        TblVenta objetoVenta = new TblVenta();

        objetoVenta.setFechaVenta(new SimpleDateFormat("yyyy-MM-dd").format(myDate));
        objetoVenta.setPkIdVenta(objetoVenta.maximoId());
        objetoVenta.setCatalogo(new Catalogo().buscar(Integer.toString(idCatalogo)));
        objetoVenta.setTblCliente(new TblCliente().buscar(Integer.toString(idCliente)));
        objetoVenta.setTblCombo(new TblCombo().buscar(Integer.toString(idCombo)));
        //
        TblDetalle auxdetalle = new TblDetalle();
        auxdetalle.setPkIdDetalle(auxdetalle.maximoId());
        auxdetalle.setCantidadDetalle(cantidadDetalle);
        auxdetalle.setFkTipoDescuento(tipoDescuentoDetalle);
        auxdetalle.setTblProducto(new TblProducto().buscar(idproducto));
        auxdetalle.insertar(auxdetalle);
        //
        objetoVenta.setTblDetalle(auxdetalle);
        //
        objetoVenta.setTblFormaPago(new TblFormaPago().buscar(Integer.toString(idFormaPago)));
        objetoVenta.setTblVendedor(new TblVendedor().buscar(Integer.toString(idVendedor)));

        objetoVenta.insertar(objetoVenta);


        /*  objeto.insertar(
                
                1, //      int idCatalogo,
                3, //      idCliente
                1, //      idCombo
                222,//     idproducto
                7, //     cantidadDetalle
                1, //      tipoDescuentoDetalle
                1, //      idFormaPago
                30 //     tblVendedor
                );*/
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
