package entidades;
// Generated 10-12-2019 22:11:16 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * TblTipoDescuento generated by hbm2java
 */
public class TblTipoDescuento implements java.io.Serializable {

    private Integer pkIdTipo;
    private int nombreTipo;
    private float porcentajeDescuento;
    private Set tblCombos = new HashSet(0);

    public TblTipoDescuento() {
    }

    public TblTipoDescuento(int nombreTipo, float porcentajeDescuento) {
        this.nombreTipo = nombreTipo;
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public TblTipoDescuento(int nombreTipo, float porcentajeDescuento, Set tblCombos) {
        this.nombreTipo = nombreTipo;
        this.porcentajeDescuento = porcentajeDescuento;
        this.tblCombos = tblCombos;
    }

    public Integer getPkIdTipo() {
        return this.pkIdTipo;
    }

    public void setPkIdTipo(Integer pkIdTipo) {
        this.pkIdTipo = pkIdTipo;
    }

    public int getNombreTipo() {
        return this.nombreTipo;
    }

    public void setNombreTipo(int nombreTipo) {
        this.nombreTipo = nombreTipo;
    }

    public float getPorcentajeDescuento() {
        return this.porcentajeDescuento;
    }

    public void setPorcentajeDescuento(float porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public Set getTblCombos() {
        return this.tblCombos;
    }

    public void setTblCombos(Set tblCombos) {
        this.tblCombos = tblCombos;
    }

    @Override
    public String toString() {
        return "TblTipoDescuento{" + "pkIdTipo=" + pkIdTipo
                + ", nombreTipo=" + nombreTipo
                + ", porcentajeDescuento=" + porcentajeDescuento
                + '}';
    }

}
