<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.List"%>
<%@ page language="java"%>
<%@include file="head.jsp"%>


<%@page import="java.lang.String"%>
<%@page import="entidades.TblProducto"%>

<%

    TblProducto tblProducto = new TblProducto();
    List<TblProducto> lista = tblProducto.Listar();


%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>Insertar Venta
            <br>
            <a href="venta.jsp">Venta</a>   |
            <a href="regcliente.jsp">Registrar Cliente</a>  |
            <a href="home.jsp">Home</a>  |
            <a href="logout.jsp">Cerrar sesion</a>  |
        </div>
        <div class="container">
            <form>

                <table border="0" class="table">
                    <thead>
                        <tr>
                            <th>Id Producto</th>
                            <th>Nombre Familia</th>

                            <th>Nombre Producto</th>

                            <th>Stock Producto</th>
                            <th>Precio Producto</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%                                                /*      
                            1, //      int idCatalogo,
                            3, //      idCliente
                            1, //      idCombo
                            222,//     idproducto
                            7, //     cantidadDetalle
                            1, //      tipoDescuentoDetalle
                            1, //      idFormaPago
                            30 //     tblVendedor
                            ); // fechaVenta
                            
                             contenido.getTblFamilia().getNombreFamilia()
                             contenido.getStockInicial()
                             */
                            for (TblProducto contenido : lista) {%>   
                        <tr>
                            <td><input type="text" id="id" value="<%= contenido.getPkIdProducto()%>"</td>

                            <td><input type="text" id="id" value="<%= contenido.getTblProveedor().getNombreProveedor()%>"</td>
                            <td><input type="text" id="id" value="<%= contenido.getNombreProducto()%>"</td>

                            <td><input type="text" id="id" value="<%= contenido.getStockProducto()%>"</td>
                            <td><input type="text" id="id" value="<%= contenido.getPrecioProducto()%>"</td> 

                        </tr>
                        <% }%> 
                        <tr>
                            <td colspan="7"><input id="submit" class="btn btn-primary col-sm-12" type="button" value="Guardar"></td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <div id="tabla"></div>    
        </div>
    </body>

    <script>
        $('input').attr('readonly', true)
        $(document).ready(function () {
            $('#submit').click(function (event) {

                var operacion = $('#operacion').val();
                var id = $('#id').val();
                var nombre = $('#nombre').val();
                var apellido = $('#apellido').val();
                var clave = $('#clave').val();
                // Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
                $.get('ControladorTblCliente', {
                    operacion: operacion,
                    id: id,
                    nombre: nombre,
                    apellido: apellido,
                    clave: clave
                }, function (responseText) {
                    $('#tabla').html(responseText);
                    console.log(responseText)

                });
            });
        });
    </script>
</html>