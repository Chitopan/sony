<%@ page language="java"%>
<%@include file="head.jsp"%>

<html>
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>Insertar Cliente
            <br>
            <a href="venta.jsp">Venta</a>   |
            <a href="regcliente.jsp">Registrar Cliente</a>  |
            <a href="home.jsp">Home</a>  |
            <a href="logout.jsp">Cerrar sesion</a>  |
        </div>
        <div class="container">
            <form>

                <table border="0" class="table">
                    <thead>
                        <tr>

                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Clave</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                    <input type="hidden" id="operacion" value="insertar" />    

                    <td><input type="text" id="nombre"></td>
                    <td><input type="text" id="apellido"></td>
                    <td><input type="text" id="clave"></td>
                    <td>
                        <SELECT nombre="estado" id="estado">
                            <OPTION value="1" >activado</OPTION>
                            <OPTION value="0" >desactivado</OPTION>
                        </SELECT>
                    </td>
                    </tr>
                    <tr>
                        <td colspan="4"><input id="submit" class="btn btn-primary col-sm-12" type="button" value="Guardar"></td>
                    </tr>
                    </tbody>
                </table>
            </form>
            <div id="tabla"></div>    
        </div>
    </body>

    <script>
        $(document).ready(function () {
            $('#submit').click(function (event) {

                var operacion = $('#operacion').val();
                var id = $('#id').val();
                var nombre = $('#nombre').val();
                var apellido = $('#apellido').val();
                var clave = $('#clave').val();
                var estado = $('#estado').val();
                // Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
                $.get('ControladorTblCliente', {
                    operacion: operacion,
                    id: id,
                    nombre: nombre,
                    apellido: apellido,
                    clave: clave,
                    estado: estado
                }, function (responseText) {
                    $('#tabla').html(responseText);
                    console.log(responseText)

                });
            });
        });
    </script>
</html>